﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Xml.Linq;
using CurrencyConverter.Core.Entity;
using CurrencyConverter.Core.Enum;
using Portal.Data.Services;

namespace CurrencyConverterTest
{
    public class TestClass
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
    }
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var service = new CurrencyConverterService();
            var query = new GivenDateConvertionData()
            {
                Sum = 100,
                CurrencyFrom = CurrencyCode.EUR,
                CurrencyTo = CurrencyCode.USD,
                Date = DateTime.Now.AddDays(-1)
            };

            var a = new GivenDateConvertionData();

            var resultSingle = service.GetAmountByDailyExchangeQuery(query);
            var result = service.GetRatesForTimeSpan<GivenDateCurrencyRates>(DateTime.Now.AddDays(-5), DateTime.Now);
        }
    }
}
