﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurrencyConverter.Core.Interface;
using Portal.Data.Services;

namespace CurrencyConverter.Factory
{
    /// <summary>
    /// Factory for creating specific service. Would be modified if other services will be created.
    /// </summary> 
    public class CurrencyConverterServiceFactory
    {
        public static ICurrencyConverterService GetCurrencyConverterService(ServiceType type)
        {
            return new CurrencyConverterService();
        }
    }
}
