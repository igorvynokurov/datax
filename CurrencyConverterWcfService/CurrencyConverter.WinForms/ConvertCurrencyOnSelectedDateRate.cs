﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CurrencyConverter.Core.Enum;
using CurrencyConverter.WinForms.CurrencyWebService;

namespace CurrencyConverter.WinForms
{
    public partial class ConvertCurrencyOnSelectedDateRate : Form
    {
        private CurrencyConverterWebServiceClient _service;
        private CurrencyConverterWebServiceClient Service
        {
            get { return _service = _service ?? new CurrencyConverterWebServiceClient(); }
        }

        public ConvertCurrencyOnSelectedDateRate()
        {
            InitializeComponent();
            var enumList = Enum.GetValues(typeof(CurrencyCode)).Cast<CurrencyCode>().Select(x => x.ToString()).ToList();
            cbFrom.DataSource = enumList;
            cbTo.DataSource = enumList.ToList();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var date = calDate.SelectionRange.Start.Date;
                var currencyFrom = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), cbFrom.SelectedItem.ToString());
                var currencyTo = (CurrencyCode)Enum.Parse(typeof(CurrencyCode), cbTo.SelectedItem.ToString());
                var amount = numAmount.Value;

                var res = Service.GetAmountByDailyExchangeQuery(new GivenDateConvertionData()
                {
                    Sum = amount,
                    CurrencyFrom = currencyFrom,
                    CurrencyTo = currencyTo,
                    Date = date
                });
                MessageBox.Show(String.Format("On {0} {1} {2} = {3} {4}", date.ToString("M"), amount, currencyFrom, res, currencyTo));
            }
            catch (System.ServiceModel.FaultException er)
            {
                MessageBox.Show(er.Message);
            }
            catch (Exception er)
            {
                MessageBox.Show("Unexpected error occurred");
            }
        }
    }
}
