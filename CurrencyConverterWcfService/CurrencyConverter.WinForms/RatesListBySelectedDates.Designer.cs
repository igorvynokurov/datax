﻿namespace CurrencyConverter.WinForms
{
    partial class RatesListBySelectedDates
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.calTo = new System.Windows.Forms.MonthCalendar();
            this.lblTo = new System.Windows.Forms.Label();
            this.btnApply = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.calFrom = new System.Windows.Forms.MonthCalendar();
            this.cbCurrency = new System.Windows.Forms.ComboBox();
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblCurrencyList = new System.Windows.Forms.Label();
            this.gvResult = new System.Windows.Forms.DataGridView();
            this.btnShowRateOnSelectedDate = new System.Windows.Forms.Button();
            this.lblList = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gvResult)).BeginInit();
            this.SuspendLayout();
            // 
            // calTo
            // 
            this.calTo.Location = new System.Drawing.Point(288, 45);
            this.calTo.MaxSelectionCount = 1;
            this.calTo.Name = "calTo";
            this.calTo.TabIndex = 1;
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(313, 20);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(82, 13);
            this.lblTo.TabIndex = 4;
            this.lblTo.Text = "Search rates to:";
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(231, 236);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(111, 23);
            this.btnApply.TabIndex = 6;
            this.btnApply.Text = "Search";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(370, 236);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(109, 23);
            this.btnExport.TabIndex = 7;
            this.btnExport.Text = "Export to CSV";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // calFrom
            // 
            this.calFrom.Location = new System.Drawing.Point(35, 45);
            this.calFrom.MaxSelectionCount = 1;
            this.calFrom.Name = "calFrom";
            this.calFrom.ShowToday = false;
            this.calFrom.ShowTodayCircle = false;
            this.calFrom.TabIndex = 0;
            // 
            // cbCurrency
            // 
            this.cbCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCurrency.FormattingEnabled = true;
            this.cbCurrency.Location = new System.Drawing.Point(89, 236);
            this.cbCurrency.Name = "cbCurrency";
            this.cbCurrency.Size = new System.Drawing.Size(111, 21);
            this.cbCurrency.TabIndex = 2;
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(53, 20);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(93, 13);
            this.lblFrom.TabIndex = 3;
            this.lblFrom.Text = "Search rates from:";
            // 
            // lblCurrencyList
            // 
            this.lblCurrencyList.AutoSize = true;
            this.lblCurrencyList.Location = new System.Drawing.Point(23, 239);
            this.lblCurrencyList.Name = "lblCurrencyList";
            this.lblCurrencyList.Size = new System.Drawing.Size(60, 13);
            this.lblCurrencyList.TabIndex = 5;
            this.lblCurrencyList.Text = "Currencies:";
            // 
            // gvResult
            // 
            this.gvResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvResult.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gvResult.Location = new System.Drawing.Point(0, 326);
            this.gvResult.Name = "gvResult";
            this.gvResult.RowHeadersVisible = false;
            this.gvResult.RowHeadersWidth = 10;
            this.gvResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.gvResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvResult.Size = new System.Drawing.Size(500, 462);
            this.gvResult.TabIndex = 8;
            // 
            // btnShowRateOnSelectedDate
            // 
            this.btnShowRateOnSelectedDate.Location = new System.Drawing.Point(358, 288);
            this.btnShowRateOnSelectedDate.Name = "btnShowRateOnSelectedDate";
            this.btnShowRateOnSelectedDate.Size = new System.Drawing.Size(130, 23);
            this.btnShowRateOnSelectedDate.TabIndex = 9;
            this.btnShowRateOnSelectedDate.Text = "Rate on selected date";
            this.btnShowRateOnSelectedDate.UseVisualStyleBackColor = true;
            this.btnShowRateOnSelectedDate.Click += new System.EventHandler(this.btnShowRateOnSelectedDate_Click);
            // 
            // lblList
            // 
            this.lblList.AutoSize = true;
            this.lblList.Location = new System.Drawing.Point(7, 298);
            this.lblList.Name = "lblList";
            this.lblList.Size = new System.Drawing.Size(142, 13);
            this.lblList.TabIndex = 10;
            this.lblList.Text = "Result list for selected dates:";
            // 
            // RatesListBySelectedDates
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 788);
            this.Controls.Add(this.lblList);
            this.Controls.Add(this.btnShowRateOnSelectedDate);
            this.Controls.Add(this.lblCurrencyList);
            this.Controls.Add(this.lblFrom);
            this.Controls.Add(this.gvResult);
            this.Controls.Add(this.cbCurrency);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.calFrom);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.calTo);
            this.Name = "RatesListBySelectedDates";
            this.Text = "Currency converter";
            ((System.ComponentModel.ISupportInitialize)(this.gvResult)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar calTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.MonthCalendar calFrom;
        private System.Windows.Forms.ComboBox cbCurrency;
        private System.Windows.Forms.Label lblFrom;
        private System.Windows.Forms.Label lblCurrencyList;
        private System.Windows.Forms.DataGridView gvResult;
        private System.Windows.Forms.Button btnShowRateOnSelectedDate;
        private System.Windows.Forms.Label lblList;
    }
}

