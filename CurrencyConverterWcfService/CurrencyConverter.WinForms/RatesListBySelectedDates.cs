﻿using System;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Windows.Forms;
using CurrencyConverter.Core.Enum;
using CurrencyConverter.WinForms.CurrencyWebService;

namespace CurrencyConverter.WinForms
{
    public partial class RatesListBySelectedDates : Form
    {
        private CurrencyConverterWebServiceClient _service;
        private CurrencyConverterWebServiceClient Service
        {
            get { return _service = _service ?? new CurrencyConverterWebServiceClient(); }
        }

        public RatesListBySelectedDates()
        {
            InitializeComponent();
            var enumList = Enum.GetValues(typeof (CurrencyCode)).Cast<CurrencyCode>().Select(x => x.ToString()).ToList();
            enumList.Insert(0, "");
            cbCurrency.DataSource = enumList;
        }
        
        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                CurrencyCode? currency = String.IsNullOrEmpty(cbCurrency.SelectedItem.ToString())
                    ? (CurrencyCode?)null
                    : (CurrencyCode)Enum.Parse(typeof(CurrencyCode), cbCurrency.SelectedItem.ToString());

                var currencyShowDate = currency ?? CurrencyCode.USD;
                var result = GetGivenDateCurrencyRates();

                var viewResult = result.SelectMany(x => x.Rates, (data, rate) => new { data, rate }).Select(x => new
                {
                    Date = x.rate.Key == currencyShowDate ? x.data.Date.ToString("d") : "",
                    Currency = x.rate.Key,
                    Rate = x.rate.Value
                }).ToList();

                gvResult.DataSource = viewResult;
            }
            catch (FaultException er)
            {
                MessageBox.Show(er.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Unexpected error occurred");
            }
        }

        private void btnShowRateOnSelectedDate_Click(object sender, EventArgs e)
        {
            ConvertCurrencyOnSelectedDateRate formConvert = new ConvertCurrencyOnSelectedDateRate();
            formConvert.Show();
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            try
            {
                CsvExport csv = new CsvExport(GetGivenDateCurrencyRates().ToList());
                var currency = String.IsNullOrEmpty(cbCurrency.SelectedItem.ToString())
                    ? "All"
                    : cbCurrency.SelectedItem.ToString();

                string fileName = String.Format("{0}_{1}_{2}.csv", currency, calFrom.SelectionRange.Start.ToString("d"),
                    calTo.SelectionRange.Start.ToString("d"));
                var fullPath = csv.ExportToFile(fileName);
                MessageBox.Show(fullPath + " was created successfully");
            }
            catch (IOException)
            {
                MessageBox.Show("Cannot create CSV file. Close all opened csv files.");
            }
            catch (FaultException er)
            {
                MessageBox.Show(er.Message);
            }
            catch (Exception)
            {
                MessageBox.Show("Cannot create CSV file");
            }
        }

        private GivenDateCurrencyRates[] GetGivenDateCurrencyRates()
        {
            var dateStart = calFrom.SelectionRange.Start;
            var dateEnd = calTo.SelectionRange.Start;
            return Service.GetRatesForTimeSpan(dateStart, dateEnd, SelectedCurrencyCode);
        }

        private CurrencyCode? SelectedCurrencyCode
        {
            get
            {
                return String.IsNullOrEmpty(cbCurrency.SelectedItem.ToString())
                    ? (CurrencyCode?)null
                    : (CurrencyCode)Enum.Parse(typeof(CurrencyCode), cbCurrency.SelectedItem.ToString());
            }
        }
    }
}
