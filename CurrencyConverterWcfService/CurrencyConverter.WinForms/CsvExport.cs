﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using CurrencyConverter.WinForms.CurrencyWebService;

namespace CurrencyConverter.WinForms
{
    public class CsvExport
    {
        private IList<GivenDateCurrencyRates> _list;

        public CsvExport(IList<GivenDateCurrencyRates> list)
        {
            _list = list;
        }

        public string GetExportData()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var obj in _list)
            {
                sb.Append(obj.Date.ToString("d")).Append(";");
                foreach (var r in obj.Rates)
                {
                    sb.Append(String.Format("{0}:{1}", r.Key, r.Value)).Append(";");
                }
                sb.Remove(sb.Length - 1, 1).AppendLine();
            }
            return sb.ToString();
        }
        
        public string ExportToFile(string path)
        {
            File.WriteAllText(path, GetExportData());
            return System.IO.Path.GetFullPath(path);
        }
    }
}
