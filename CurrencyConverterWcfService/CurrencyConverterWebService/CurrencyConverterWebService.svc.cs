﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using CurrencyConverter.Core.Enum;
using CurrencyConverter.Core.Interface;
using CurrencyConverter.Factory;

namespace CurrencyConverterWebService
{
    public class CurrencyConverterWebService : ICurrencyConverterWebService
    {
        private ICurrencyConverterService CurrencyService
        {
            get
            {
                try
                {
                    var type =
                    (ServiceType)Enum.Parse(typeof(ServiceType), ConfigurationManager.AppSettings["ServiceType"]);
                    return CurrencyConverterServiceFactory.GetCurrencyConverterService(type);
                }
                catch (ArgumentException er)
                {
                    //todo add log to check web config 
                }
                return CurrencyConverterServiceFactory.GetCurrencyConverterService(ServiceType.EuropeanCentralBankWebXml);
            }
        }
        
        public decimal GetAmountByDailyExchangeQuery(GivenDateConvertionData query)
        {
            try
            {
                return CurrencyService.GetAmountByDailyExchangeQuery(query);
            }
            catch (ApplicationException er)
            {
               throw new FaultException(er.Message);
            }
            catch (Exception er)
            {
                throw new FaultException("Unexpected server error.");
                //todo add log
            }
        }

        public IList<GivenDateCurrencyRates> GetRatesForTimeSpan(DateTime startDate, DateTime endDate, CurrencyCode? currencyCode = null)
        {
            try
            {
                return CurrencyService.GetRatesForTimeSpan<GivenDateCurrencyRates>(startDate, endDate, currencyCode);
            }
            catch (ApplicationException er)
            {
                throw new FaultException(er.Message);
            }
            catch (Exception er)
            {
                var t = er;
                throw new FaultException("Unexpected server error.");
                //todo add log
            }
        }
    }
}
