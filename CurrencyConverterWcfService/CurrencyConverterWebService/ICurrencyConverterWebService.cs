﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Services;
using CurrencyConverter.Core.Entity;
using CurrencyConverter.Core.Enum;

namespace CurrencyConverterWebService
{
    [ServiceContract]
    [WebService(Description = "Service for currency rates receiving, currency convertion for given date")]
    public interface ICurrencyConverterWebService
    {
        [OperationContract]
        [WebMethod(Description = "The conversion of sum X in currency A into the sum Y in currency B at the given date.")]
        decimal GetAmountByDailyExchangeQuery(GivenDateConvertionData query);

        [OperationContract]
        [WebMethod(Description = "Receiving of the currency rates for the given time period")]
        IList<GivenDateCurrencyRates> GetRatesForTimeSpan(DateTime startDate, DateTime endDate, CurrencyCode? currencyCode = null);
    }


    [DataContract]
    public class GivenDateConvertionData : IGivenDateConvertionData
    {
        private DateTime _date = DateTime.Now.AddDays(-1).Date;
        [DataMember]
        public DateTime Date
        {
            get { return _date; }
            set { _date = value.Date; }
        }
        [DataMember]
        public CurrencyCode CurrencyFrom { get; set; }
        [DataMember]
        public CurrencyCode CurrencyTo { get; set; }
        [DataMember]
        public decimal Sum { get; set; }
    }

    [DataContract]
    public class GivenDateCurrencyRates : IGivenDateCurrencyRates
    {
        [DataMember]
        public DateTime Date { get; set; }
        [DataMember]
        public IDictionary<CurrencyCode, decimal> Rates { get; set; }
    }
}
