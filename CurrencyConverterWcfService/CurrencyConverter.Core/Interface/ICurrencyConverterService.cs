﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurrencyConverter.Core.Entity;
using CurrencyConverter.Core.Enum;

namespace CurrencyConverter.Core.Interface
{
    /// <summary>
    /// Service for currency rates receiving, currency convertion for given date
    /// </summary> 
    public interface ICurrencyConverterService
    {
        /// <summary>
        /// The conversion of sum X in currency A into the sum Y in currency B at the given date.
        /// <param name="query">Date convertion data</param>
        /// </summary> 
        /// <returns>Sum of convertion</returns>
        decimal GetAmountByDailyExchangeQuery(IGivenDateConvertionData query);

        /// <summary>
        /// Receiving of the currency rates for the given time period.
        /// <param name="startDate">First date of given time period</param>
        /// <param name="endDate">Last date of given time period</param>
        /// <param name="currencyCode">Specific currency code</param>
        /// </summary> 
        /// <returns>Specific currency rate info for given dates or info for all currencies if currencyCode is not given</returns>
        IList<T> GetRatesForTimeSpan<T>(DateTime startDate, DateTime endDate, CurrencyCode? currencyCode = null)
            where T : class, IGivenDateCurrencyRates, new();
    }
}
