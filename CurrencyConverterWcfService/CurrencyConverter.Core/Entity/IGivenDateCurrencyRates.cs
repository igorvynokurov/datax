﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CurrencyConverter.Core.Enum;

namespace CurrencyConverter.Core.Entity
{
    /// <summary>
    /// Interface to describe the result of receiving of the currency rates for the given time period.
    /// </summary>   
    public interface IGivenDateCurrencyRates
    {
        /// <summary>
        /// Date with specific currency rates
        /// </summary>
        DateTime Date { get; set; }
        /// <summary>
        /// Dictionary of rates for currency codes Key - CurrencyCode, Value - rate
        /// </summary>   
        IDictionary<CurrencyCode, decimal> Rates { get; set; }
    }

    public class GivenDateCurrencyRates : IGivenDateCurrencyRates
    {
        public DateTime Date { get; set; }
        public IDictionary<CurrencyCode, decimal> Rates { get; set; }
    }
}
