﻿using System;
using CurrencyConverter.Core.Enum;

namespace CurrencyConverter.Core.Entity
{
    /// <summary>
    /// Interface to describe properties of the conversion of sum X in currency A into the sum Y in currency B at the given date.
    /// </summary>   
    public interface IGivenDateConvertionData
    {
        /// <summary>
        /// Currency A to convert from
        /// </summary>     
        CurrencyCode CurrencyFrom { get; set; }
        /// <summary>
        /// Currency B to convert to
        /// </summary>     
        CurrencyCode CurrencyTo { get; set; }
        /// <summary>
        /// The given date to get rate
        /// </summary>     
        DateTime Date { get; set; }
        /// <summary>
        /// Sum of convertion in Currency A
        /// </summary>     
        decimal Sum { get; set; }
    }

    public class GivenDateConvertionData : IGivenDateConvertionData
    {
        private DateTime _date = DateTime.Now.AddDays(-1).Date;
        public DateTime Date
        {
            get { return _date; }
            set { _date = value.Date; }
        }

        public CurrencyCode CurrencyFrom { get; set; }
        public CurrencyCode CurrencyTo { get; set; }
        public decimal Sum { get; set; }
    }
}
