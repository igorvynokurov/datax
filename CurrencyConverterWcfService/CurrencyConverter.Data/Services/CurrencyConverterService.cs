﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using CurrencyConverter.Core.Entity;
using CurrencyConverter.Core.Enum;
using CurrencyConverter.Core.Interface;

namespace Portal.Data.Services
{
    public class CurrencyConverterService : ICurrencyConverterService
    {
        private const string Url = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml";
        private const string Namespace = "http://www.ecb.int/vocabulary/2002-08-01/eurofxref";
        private const string ElementName = "Cube";

        private XDocument _source;
        private IEnumerable<XElement> _data;

        public CurrencyConverterService()
        {
            try
            {
                _source = XDocument.Load(Url);
                XNamespace ns = Namespace;
                _data = _source.Descendants(ns + ElementName);
            }
            catch (WebException er)
            {
                throw new ApplicationException("Cannot find data source. Please try again later");
            }
            catch (XmlException)
            {
                throw new ApplicationException("Cannot access data source.");
            }
        }

        public decimal GetAmountByDailyExchangeQuery(IGivenDateConvertionData query)
        {
            try
            {
                var dateElement = _data.First(IsDateElementWithConcreteDate(query.Date));
                var rateFrom = GetRateValueFromXElement(dateElement, query.CurrencyFrom);
                var rateTo = GetRateValueFromXElement(dateElement, query.CurrencyTo);
                return rateTo * query.Sum / rateFrom;
            }
            catch (InvalidOperationException)
            {
                throw new ApplicationException("No rate info was found for this date");
            }
        }

        public IList<T> GetRatesForTimeSpan<T>(DateTime start, DateTime end, CurrencyCode? currencyCode = null)
            where T : class, IGivenDateCurrencyRates, new()
        {
            if (end < start)
            {
                throw new ApplicationException("End date value must be greater than start");
            }
            var dateElements = _data.Where(IsDateElementBetweenTwoDates(start, end));
            var result = dateElements.Select(x => new T
            {
                Date = DateTime.Parse(x.Attribute("time").Value),
                Rates =
                    x.Elements()
                        .Where(IsCurrencyRateElement(currencyCode))
                        .ToDictionary(e => (CurrencyCode)Enum.Parse(typeof(CurrencyCode), e.Attribute("currency").Value),
                            e => decimal.Parse(e.Attribute("rate").Value, CultureInfo.InvariantCulture))
            }).ToList();
            if (!result.Any())
            {
                throw new ApplicationException("Nothing was found for selected dates");
            }
            return result;
        }

        private decimal GetRateValueFromXElement(XElement dateElement, CurrencyCode currencyCode)
        {
            if (currencyCode == CurrencyCode.EUR)
            {
                return 1;
            }
            var rateElement = dateElement.Elements().First(IsCurrencyRateElement(currencyCode));
            return Decimal.Parse(rateElement.Attribute("rate").Value, CultureInfo.InvariantCulture);
        }

        private Func<XElement, bool> IsDateElementBetweenTwoDates(DateTime start, DateTime end)
        {
            return x =>
            {
                if (x.Attribute("time") == null)
                {
                    return false;
                }
                var current = DateTime.Parse(x.Attribute("time").Value);
                return current >= start && current <= end;
            };
        }

        private Func<XElement, bool> IsDateElementWithConcreteDate(DateTime date)
        {
            return x =>
            {
                if (x.Attribute("time") == null)
                {
                    return false;
                }
                var current = DateTime.Parse(x.Attribute("time").Value);
                return current.Date == date.Date;
            };
        }

        private Func<XElement, bool> IsCurrencyRateElement(CurrencyCode? currencyCode)
        {
            var selectAll = !currencyCode.HasValue;
            return x =>
            {
                var cur = x.Attribute("currency").Value;
                return selectAll || cur == currencyCode.Value.ToString();
            };
        }
    }
}